## sys_oplus_mssi_64_cn-user 11 RP1A.200720.011 1627567766349 release-keys
- Manufacturer: alps
- Platform: mt6893
- Codename: oplus6893
- Brand: alps
- Flavor: sys_oplus_mssi_64_cn-user
- Release Version: 11
- Id: RP1A.200720.011
- Incremental: 1627567766349
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: alps/vnd_oplus6893/oplus6893:11/RP1A.200720.011/1627567766349:user/release-keys
- OTA version: 
- Branch: sys_oplus_mssi_64_cn-user-11-RP1A.200720.011-1627567766349-release-keys
- Repo: alps_oplus6893_dump


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
